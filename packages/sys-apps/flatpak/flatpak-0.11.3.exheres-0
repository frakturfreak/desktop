# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ suffix=tar.xz release=${PV} ] autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require test-dbus-daemon
require utf8-locale

SUMMARY="Linux application sandboxing and distribution framework"
HOMEPAGE="http://flatpak.org/"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.1.2
        app-text/docbook-xsl-stylesheets
        app-text/xmlto
        dev-libs/libxslt
        sys-devel/gettext[>=0.18.2]
        sys-devel/libtool
        virtual/pkg-config[>=0.24]
        gtk-doc? ( dev-doc/gtk-doc[>=1.20] )
    build+run:
        app-arch/libarchive[>=2.8.0]
        app-crypt/gpgme[>=1.1.8]
        core/json-glib
        dev-libs/appstream-glib[>=0.5.10]
        dev-libs/glib:2[>=2.44]
        dev-libs/libxml2:2.0[>=2.4]
        gnome-desktop/libsoup:2.4
        sys-auth/polkit[>=0.98]
        sys-devel/libostree:1[>=2018.2]
        sys-libs/libseccomp
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.40.0] )
    recommendation:
        sys-apps/xdg-desktop-portal[>=0.10] [[ description = [ Most flatpaks require
            xdg-desktop-portal at runtime ] ]]
"

BUGS_TO="keruspe@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-docbook-docs
    --enable-documentation
    --enable-sandboxed-triggers
    --enable-seccomp
    --enable-system-helper
    # unstable peer to peer support, needs libostree with
    # --enable-experimental-api
    --disable-p2p
    --disable-sudo
    --disable-xauth
    # Otherwise flatpak tries to use /var/lib/lib/flatpak
    --localstatedir=/var
    --with-priv-mode=none
    --with-dbus_config_dir=/usr/share/dbus-1/system.d
    --with-system-install-dir=/var/lib/flatpak
    --without-system-bubblewrap
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    gtk-doc
)

# testlibrary and test-libglnx-fdio fail due to sydbox
RESTRICT="test"

src_prepare() {
    default

    # Don't use /var/tmp as test dir, but ${TEMP}
    edo sed -e "s:/var/tmp/test-flatpak-XXXXXX:"${TEMP%/}"/test-flatpak-XXXXXX:" -i \
        tests/libtest.sh

    # We don't have /usr/lib/locale/C.UTF8
    edo sed "/lib\/locale/d" -i tests/make-test-runtime.sh
}

src_test() {
    # for make-test-runtime.sh
    require_utf8_locale

    esandbox allow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent*"
    esandbox allow_net --connect "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent*"

    test-dbus-daemon_src_test

    esandbox disallow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent*"
    esandbox disallow_net --connect "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent*"
}

src_install() {
    default

    keepdir /var/lib/flatpak
}

