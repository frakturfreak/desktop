# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user='flatpak' release=${PV} suffix='tar.xz' ]

SUMMARY="A portal frontend service for Flatpak"
DESCRIPTION="
xdg-desktop-portal works by exposing a series of D-Bus interfaces known as
portals under a well-known name and object path. The portal interfaces include
APIs for file access, opening URIs, printing and others.
Flatpak grants sandboxed applications talk access to names in the
org.freedesktop.portal.* prefix. One possible way to use the portal APIs is
thus just to make D-Bus calls. For many of the portals, toolkits (e.g. GTK+)
are expected to support portals transparently if you use suitable high-level
APIs.
To actually use most portals, xdg-desktop-portal relies on a backend that
provides implementations of the org.freedesktop.impl.portal.* interfaces. One
such backend is provided by xdg-desktop-portal-gtk. Another one is in
development here: xdg-desktop-portal-kde."

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="screencast [[ description = [ Enable the screencast D-Bus interface ( portal ) ] ]]"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.3
        app-text/xmlto
        sys-devel/gettext[>=0.18.3]
        virtual/pkg-config[>=0.24]
    build+run:
        dev-libs/glib:2
        sys-apps/flatpak
        !sys-apps/flatpak[<0.11] [[
            description = [ file collisions, document portal was moved into xdg-desktop-portal ]
            resolution = upgrade-blocked-before
        ]]
        screencast? ( media/pipewire[>=0.1.8] )
    suggestion:
        kde/xdg-desktop-portal-kde  [[
            description = [ Backend providing integration with the Plasma desktop ]
        ]]
        sys-apps/xdg-desktop-portal-gtk [[
            description = [ Backend providing integration with GTK desktops such as GNOME ]
        ]]
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'screencast pipewire'
)

