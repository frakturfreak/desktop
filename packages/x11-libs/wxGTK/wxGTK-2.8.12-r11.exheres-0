# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require wxGTK [ supported_gtk=[ 2 ] ]

PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    gstreamer joystick odbc sdl tiff
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/expat
        dev-libs/glib:2
        media-libs/libpng:=
        x11-dri/glu
        x11-dri/mesa
        x11-libs/gtk+:2
        x11-libs/libSM
        x11-libs/libXxf86vm
        x11-libs/libXpm
        x11-libs/pango [[ note = [ required for unicode support ] ]]
        gstreamer? (
            gnome-platform/GConf:2
            media-libs/gstreamer:0.10
            media-plugins/gst-plugins-base:0.10
        )
        odbc? ( dev-db/unixODBC )
        sdl? ( media-libs/SDL:0 )
        tiff? ( media-libs/tiff )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2.8-Avoid-collisions.patch
    "${FILES}"/${PN}-2.8.12-fix-gcc6-compilation.patch
)

src_prepare() {
    wxGTK_src_prepare

    edo sed -e "s/pkg-config --atleast-pkgconfig-version 0.7/\$PKG_CONFIG --atleast-pkgconfig-version 0.7/" \
            -e "s#/usr/lib#/usr/$(exhost --target)/lib#g" \
            -e "s#lib64#lib#g" \
            -i configure
}

configure_one_multibuild() {
    econf \
        PKG_CONFIG="/usr/$(exhost --target)/bin/$(exhost --tool-prefix)pkg-config" \
        --hates=datarootdir \
        --hates=docdir \
        --enable-compat26 \
        --enable-controls \
        --enable-expat=sys \
        --enable-graphics_ctx \
        --enable-gui \
        --enable-intl \
        --enable-shared \
        --enable-threads \
        --enable-unicode \
        $(if exhost --is-native -q;then
            echo --enable-precomp-headers
        else
            echo --disable-precomp-headers
        fi) \
        --with-gtk=${GTK_VERSION} \
        --with-libjpeg=sys \
        --with-libpng=sys \
        --with-libxpm=sys \
        --with-opengl \
        --with-regex=builtin \
        --with-zlib=sys \
        --without-gnomeprint \
        --without-gnomevfs \
        --without-motif \
        --without-wine \
        $(option_enable joystick) \
        $(option_enable gstreamer mediactrl) \
        $(option_with odbc odbc sys) \
        $(option_with sdl) \
        $(option_with tiff libtiff sys)
}

compile_one_multibuild() {
    default
    emake -C contrib/src
}

install_one_multibuild() {
    default

    emake -C contrib/src DESTDIR="${IMAGE}" install

    wxGTK_handle_alternatives
}

