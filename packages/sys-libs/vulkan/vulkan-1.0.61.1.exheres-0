# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=Vulkan-LoaderAndValidationLayers tag=sdk-${PV} ]
require python [ blacklist=2 multibuild=false has_bin=false has_lib=false ] cmake [ api=2 ]

SUMMARY="Vulkan loader and validation layers "
DESCRIPTION="
Vulkan is an Explicit API, enabling direct control over how GPUs actually work. No (or very little)
validation or error checking is done inside a Vulkan driver. Applications have full control and
responsibility. Any errors in how Vulkan is used often result in a crash. This project provides
standard validation layers that can be enabled to ease development by helping developers verify
their applications correctly use the Vulkan API.
"
HOMEPAGE="https://www.khronos.org/vulkan/"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="X wayland"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        X? (
            x11-libs/libxcb
            x11-libs/libX11
        )
        wayland? ( sys-libs/wayland )
"

BUGS_TO="keruspe@exherbo.org"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_LOADER:BOOL=TRUE
    -DBUILD_VKJSON:BOOL=TRUE
    # Those need glsl and spirv
    -DBUILD_DEMOS:BOOL=FALSE
    -DBUILD_TESTS:BOOL=FALSE
    -DBUILD_LAYERS:BOOL=FALSE
    -DBUILD_WSI_MIR_SUPPORT:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    "X BUILD_WSI_XCB_SUPPORT"
    "X BUILD_WSI_XLIB_SUPPORT"
    "wayland BUILD_WSI_WAYLAND_SUPPORT"
)

src_install() {
    cmake_src_install
    keepdir /etc/vulkan/icd.d
}
