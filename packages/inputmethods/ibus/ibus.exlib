# Copyright 2009-2013 Hong Hao <oahong@oahong.me>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'ibus-1.1.0.20090211.ebuild' from Gentoo, which is:
#    Copyright 1999-2009 Gentoo Foundation

require gtk-icon-cache gsettings github \
    python [ blacklist=none multibuild=false ] \
    vala [ with_opt=true vala_dep=true ] \
    option-renames [ renames=[ 'gtk3 gtk' ] ]

export_exlib_phases src_install pkg_postinst pkg_postrm

SUMMARY="Intelligent Input Bus"
DESCRIPTION="IBus is an Intelligent Input Bus. It is a new input framework for Linux OS.
It provides full featured and user friendly input method user interface.
It also may help developers to develop input method easily."

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="gobject-introspection gtk gtk-doc wayland X
    gtk? (
        ( providers: gtk2 [[ description = [ Build GTK+ 2 immodule ] ]]
                     gtk3 [[ description = [ Build GTK+ 3 immodule ] ]]
        ) [[ number-selected = at-least-one ]]
    )
    vapi [[ requires = gobject-introspection ]]
    ( linguas: ar as bg bn_IN ca da de en_GB es et eu fa fr gu hi hu ia it ja kn ko lv
               ml mr nb nl or pa pl pt_BR ru sr sr@latin ta te tg uk vi zh_CN zh_HK zh_TW
    )
"

DEPENDENCIES="
    build:
        app-doc/gtk-doc-autotools
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        app-text/cldr-emoji-annotation
        app-text/unicode-emoji
        app-text/iso-codes
        core/json-glib
        dev-libs/glib[>=2.36.0]
        dev-libs/libxml2:2.0
        gnome-bindings/pygobject:3[>=3.0.0]
        gnome-desktop/dconf[>=0.13.4]
        sys-apps/dbus
        x11-libs/libnotify[>=0.7]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        providers:gtk2? ( x11-libs/gtk+:2 )
        providers:gtk3? ( x11-libs/gtk+:3 )
        wayland? (
            sys-libs/wayland
            x11-libs/libxkbcommon
        )
        X? (
            x11-libs/gtk+:3 [[ note = [ for XIM ] ]]
            x11-libs/libX11
        )
    recommendation:
        gnome-desktop/librsvg
    suggestion:
        inputmethods/ibus-qt
        inputmethods/ibus-pinyin
        inputmethods/ibus-table
        inputmethods/ibus-chewing
        inputmethods/ibus-anthy
        inputmethods/ibus-hangul
"

RESTRICT="test" # require X and a running ibus-daemon
UPSTREAM_DOCUMENTATION="http://code.google.com/p/ibus/wiki [[ lang = en,zh_CN ]]"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --prefix=/usr
    --exec_prefix=/usr/$(exhost --target)
    --includedir=/usr/$(exhost --target)/include
    --disable-appindicator
    --disable-gconf
    --disable-memconf
    --disable-python-library
    --disable-tests
    --enable-dconf
    --enable-emoji-dict
    --enable-engine
    --enable-libnotify
    --enable-setup
    --enable-surrounding-text
    --enable-ui
    --with-unicode-emoji-dir=/usr/share/unicode/emoji
    --with-python=${PYTHON}
    --with-panel-icon-keyboard=yes
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection' 'gtk-doc'
    'providers:gtk2' 'providers:gtk3' 'vapi vala' 'wayland' 'X xim'
)

update_gtk_immodules() {
    option providers:gtk2 && gtk-query-immodules-2.0 --update-cache
    option providers:gtk3 && gtk-query-immodules-3.0 --update-cache
}

ibus_src_install() {
    default
    edo rmdir "${IMAGE}"/usr/share/${PN}/engine
}

ibus_pkg_postinst() {
    update_gtk_immodules
    gtk_icon_cache_exlib_update_theme_cache
    gsettings_pkg_postinst
}

ibus_pkg_postrm() {
    update_gtk_immodules
    gtk_icon_cache_exlib_update_theme_cache
    gsettings_pkg_postrm
}

