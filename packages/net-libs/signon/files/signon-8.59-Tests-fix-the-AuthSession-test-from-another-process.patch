Upstream: yes

From b01d837c3e0f0b65586f9cbf514b8fa020c6d672 Mon Sep 17 00:00:00 2001
From: Alberto Mardegan <alberto.mardegan@canonical.com>
Date: Wed, 24 Aug 2016 16:13:57 +0300
Subject: [PATCH] Tests: fix the AuthSession "test from another process"

This test is failing with Qt 5.6, getting stuck when calling
QDBusConnection::connectTo() from the child process. Given that calling
fork() from a test function is generally not a good idea, we refactor
this test to avoid calling fork() directly, and use a separate QProcess
instead.

While doing this, we move the refactored test to the signond-tests
directory, since this test is not testing the client library at all, but
only signond's functionality.
---
 tests/libsignon-qt-tests/testauthsession.cpp | 105 ----------------------
 tests/libsignon-qt-tests/testauthsession.h   |   1 -
 tests/signond-tests/session_tool.cpp         | 125 +++++++++++++++++++++++++++
 tests/signond-tests/session_tool.pro         |  13 +++
 tests/signond-tests/signond-tests.pro        |   5 +-
 tests/signond-tests/tst_signond.cpp          |  31 +++++++
 6 files changed, 173 insertions(+), 107 deletions(-)
 create mode 100644 tests/signond-tests/session_tool.cpp
 create mode 100644 tests/signond-tests/session_tool.pro

diff --git a/tests/libsignon-qt-tests/testauthsession.cpp b/tests/libsignon-qt-tests/testauthsession.cpp
index 6ec5efb..25e13d6 100644
--- a/tests/libsignon-qt-tests/testauthsession.cpp
+++ b/tests/libsignon-qt-tests/testauthsession.cpp
@@ -405,111 +405,6 @@ void TestAuthSession::process_with_unauthorized_method()
     QCOMPARE(errorType, SignOn::Error::MethodOrMechanismNotAllowed);
 }
 
-void TestAuthSession::process_from_other_process()
-{
-    // In order to try reusing same authentication session from
-    // another process we need the session object path, which isn't
-    // available through the client API. To work around this we
-    // don't use the API but make direct D-Bus calls instead
-
-    // The session bus used by the API cannot be accessed outside
-    // the API library so create our own bus
-    QDBusConnection dbuscon1 =
-        QDBusConnection::connectToBus(QDBusConnection::SessionBus,
-                                      "originalconnection");
-    QDBusInterface iface(SIGNOND_SERVICE,
-                         SIGNOND_DAEMON_OBJECTPATH,
-                         SIGNOND_DAEMON_INTERFACE,
-                         dbuscon1);
-
-    SlotMachine slotMachine;
-    QEventLoop sessionLoop;
-    QObject::connect(&slotMachine, SIGNAL(done()), &sessionLoop, SLOT(quit()));
-    QTimer::singleShot(10*1000, &sessionLoop, SLOT(quit()));
-
-    QVariantList arguments;
-    arguments += (quint32)SIGNOND_NEW_IDENTITY;
-    arguments += QString::fromLatin1("ssotest");
-    iface.callWithCallback(QLatin1String("getAuthSessionObjectPath"),
-                           arguments, &slotMachine,
-                           SLOT(authenticationSlot(const QString&)),
-                           SLOT(errorSlot(const QDBusError&)));
-
-    sessionLoop.exec();
-
-    QString qs;
-    if (slotMachine.m_path.isEmpty())
-        qDebug() << "getAuthSessionObjectPath failed: " << slotMachine.m_errorMessage.toLatin1().data();
-    QVERIFY(slotMachine.m_path.length() > 0);
-
-    int exitCode = 1;
-    pid_t childPid = fork();
-    QVERIFY(childPid != -1);
-
-    if (childPid != 0) {
-        int status = 0;
-        childPid = waitpid(childPid, &status, 0);
-        QVERIFY(childPid != -1 && WIFEXITED(status));
-        exitCode = WEXITSTATUS(status);
-    } else {
-        // We're in the child process now...
-        // Do not reuse existing session bus because it is seen by signond
-        // as if coming from the parent process and we want to test connection
-		 // from other process
-        QDBusConnection dbuscon2 = QDBusConnection::connectToBus(QDBusConnection::SessionBus, "otherconnection");
-        QDBusInterface *dbus = new QDBusInterface(SIGNOND_SERVICE,
-                                                  slotMachine.m_path,
-                                                  QLatin1String(SIGNOND_AUTH_SESSION_INTERFACE),
-                                                  dbuscon2);
-
-        SessionData inData;
-        inData.setSecret("testSecret");
-        inData.setUserName("testUsername");
-        QVariantMap inDataVarMap;
-        foreach(QString key, inData.propertyNames()) {
-            if (!inData.getProperty(key).isNull() && inData.getProperty(key).isValid())
-                inDataVarMap[key] = inData.getProperty(key);
-        }
-
-        arguments.clear();
-        arguments += inDataVarMap;
-        arguments += QString::fromLatin1("mech1");
-
-        QDBusMessage msg = QDBusMessage::createMethodCall(dbus->service(),
-                                                          dbus->path(),
-                                                          dbus->interface(),
-                                                          QString::fromLatin1("process"));
-        msg.setArguments(arguments);
-
-        QEventLoop processLoop;
-        QObject::connect(&slotMachine, SIGNAL(done()), &processLoop, SLOT(quit()));
-        QTimer::singleShot(10*1000, &processLoop, SLOT(quit()));
-
-        dbus->connection().callWithCallback(msg, &slotMachine,
-                                            SLOT(responseSlot(const QVariantMap&)),
-                                            SLOT(errorSlot(const QDBusError&)),
-                                            SIGNOND_MAX_TIMEOUT);
-
-        processLoop.exec();
-
-        delete dbus;
-
-        if (slotMachine.m_responseReceived) {
-            qDebug() << "AuthSession::process succeeded even though it was expected to fail";
-            exit(1);
-        } else {
-            if (slotMachine.m_errorName == SIGNOND_PERMISSION_DENIED_ERR_NAME)
-                exit(0);
-
-            qDebug() << "AuthSession::process failed but with unexpected error: " <<
-                        slotMachine.m_errorMessage;
-            exit(1);
-        }
-    }
-
-    QCOMPARE(exitCode, 0);
-}
-
 void TestAuthSession::process_many_times_after_auth()
 {
     AuthSession *as;
diff --git a/tests/libsignon-qt-tests/testauthsession.h b/tests/libsignon-qt-tests/testauthsession.h
index 251a99d..db3524c 100644
--- a/tests/libsignon-qt-tests/testauthsession.h
+++ b/tests/libsignon-qt-tests/testauthsession.h
@@ -85,7 +85,6 @@ private Q_SLOTS:
     void process_with_nonexisting_type();
     void process_with_nonexisting_method();
     void process_with_unauthorized_method();
-    void process_from_other_process();
     void process_many_times_after_auth();
     void process_many_times_before_auth();
     void process_with_big_session_data();
diff --git a/tests/signond-tests/session_tool.cpp b/tests/signond-tests/session_tool.cpp
new file mode 100644
index 0000000..5d1372b
--- /dev/null
+++ b/tests/signond-tests/session_tool.cpp
@@ -0,0 +1,125 @@
+/*
+ * This file is part of signond
+ *
+ * Copyright (C) 2016 Canonical Ltd.
+ *
+ * Contact: Alberto Mardegan <alberto.mardegan@canonical.com>
+ *
+ * This library is free software; you can redistribute it and/or
+ * modify it under the terms of the GNU Lesser General Public License
+ * version 2.1 as published by the Free Software Foundation.
+ *
+ * This library is distributed in the hope that it will be useful, but
+ * WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
+ * Lesser General Public License for more details.
+ *
+ * You should have received a copy of the GNU Lesser General Public
+ * License along with this library; if not, write to the Free Software
+ * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
+ * 02110-1301 USA
+ */
+
+#include <QCoreApplication>
+#include <QDBusConnection>
+#include <QDBusError>
+#include <QDBusInterface>
+#include <QDBusMessage>
+#include <QTextStream>
+#include <QTimer>
+
+#include "signond/signoncommon.h"
+
+class AuthSession: public QObject
+{
+    Q_OBJECT
+
+public:
+    AuthSession(const QString &path);
+    ~AuthSession();
+
+public Q_SLOTS:
+    void process(const QVariantMap &sessionData);
+    void onResponse(const QVariantMap &response);
+    void onError(const QDBusError &error);
+
+private:
+    QTextStream m_out;
+    QDBusInterface *m_dbus;
+};
+
+AuthSession::AuthSession(const QString &path):
+    QObject(),
+    m_out(stdout)
+{
+    m_dbus = new QDBusInterface(SIGNOND_SERVICE,
+                                path,
+                                QLatin1String(SIGNOND_AUTH_SESSION_INTERFACE),
+                                QDBusConnection::sessionBus());
+}
+
+AuthSession::~AuthSession()
+{
+    delete m_dbus;
+}
+
+void AuthSession::process(const QVariantMap &sessionData)
+{
+    QVariantList arguments;
+    arguments += sessionData;
+    arguments += QStringLiteral("mech1");
+
+    QDBusMessage msg = QDBusMessage::createMethodCall(m_dbus->service(),
+                                                      m_dbus->path(),
+                                                      m_dbus->interface(),
+                                                      QStringLiteral("process"));
+    msg.setArguments(arguments);
+
+    m_dbus->connection().callWithCallback(msg, this,
+                                          SLOT(onResponse(const QVariantMap&)),
+                                          SLOT(onError(const QDBusError&)),
+                                          SIGNOND_MAX_TIMEOUT);
+}
+
+void AuthSession::onResponse(const QVariantMap &response)
+{
+    // The called doesn't really care about the response value
+    Q_UNUSED(response);
+    m_out << "Response:";
+    QCoreApplication::quit();
+}
+
+void AuthSession::onError(const QDBusError &error)
+{
+    m_out << "Error:" << error.name();
+    QCoreApplication::quit();
+}
+
+int main(int argc, char **argv)
+{
+    QCoreApplication app(argc, argv);
+
+    QString sessionPath;
+
+    QStringList args = QCoreApplication::arguments();
+    for (int i = 1; i < args.count(); i++) {
+        if (args[i] == "--sessionPath") {
+            sessionPath = args[++i];
+        }
+    }
+
+    AuthSession authSession(sessionPath);
+
+    QVariantMap sessionData {
+        { "Secret", QStringLiteral("testSecret") },
+        { "UserName", QStringLiteral("testUsername") },
+    };
+    authSession.process(sessionData);
+
+    QTimer::singleShot(10*1000, &app, SLOT(quit()));
+    app.exec();
+
+    return 0;
+}
+
+#include "session_tool.moc"
diff --git a/tests/signond-tests/session_tool.pro b/tests/signond-tests/session_tool.pro
new file mode 100644
index 0000000..5b55938
--- /dev/null
+++ b/tests/signond-tests/session_tool.pro
@@ -0,0 +1,13 @@
+include(../../common-project-config.pri)
+
+TEMPLATE = app
+TARGET = session_tool
+
+QT += core dbus
+QT -= gui
+
+INCLUDEPATH += \
+    $${TOP_SRC_DIR}/lib
+
+SOURCES = \
+    session_tool.cpp
diff --git a/tests/signond-tests/signond-tests.pro b/tests/signond-tests/signond-tests.pro
index 8a90608..095f228 100644
--- a/tests/signond-tests/signond-tests.pro
+++ b/tests/signond-tests/signond-tests.pro
@@ -8,7 +8,10 @@ SUBDIRS = \
     access-control.pro \
 
 system(pkg-config --exists libqtdbusmock-1) {
-    SUBDIRS += tst_signond.pro
+    SUBDIRS += \
+        session_tool.pro \
+        tst_signond.pro
+    tst_signond.depends += session_tool
 }
 
 # Disabled until fixed
diff --git a/tests/signond-tests/tst_signond.cpp b/tests/signond-tests/tst_signond.cpp
index 2168141..8c4388f 100644
--- a/tests/signond-tests/tst_signond.cpp
+++ b/tests/signond-tests/tst_signond.cpp
@@ -22,6 +22,7 @@
 
 #include <QDebug>
 #include <QDir>
+#include <QProcess>
 #include <QSignalSpy>
 #include <QTemporaryDir>
 #include <QTest>
@@ -68,6 +69,7 @@ private Q_SLOTS:
     void testAuthSessionMechanisms_data();
     void testAuthSessionMechanisms();
     void testAuthSessionProcess();
+    void testAuthSessionProcessFromOtherProcess();
     void testAuthSessionProcessUi();
     void testAuthSessionCloseUi_data();
     void testAuthSessionCloseUi();
@@ -492,6 +494,35 @@ void SignondTest::testAuthSessionProcess()
     QCOMPARE(response, expectedResponse);
 }
 
+void SignondTest::testAuthSessionProcessFromOtherProcess()
+{
+    QDBusMessage msg = methodCall(SIGNOND_DAEMON_OBJECTPATH,
+                                  SIGNOND_DAEMON_INTERFACE,
+                                  "getAuthSessionObjectPath");
+    msg << uint(0);
+    msg << QString("ssotest");
+    QDBusMessage reply = connection().call(msg);
+    QVERIFY(replyIsValid(reply));
+
+    QCOMPARE(reply.arguments().count(), 1);
+
+    QString objectPath = reply.arguments()[0].toString();
+    QVERIFY(objectPath.startsWith('/'));
+
+    /* Pass this object path to another process, and verify that it's not
+     * allowed to use the session */
+    QProcess sessionTool;
+    sessionTool.start("./session_tool", { "--sessionPath", objectPath });
+    QVERIFY(sessionTool.waitForStarted());
+    QVERIFY(sessionTool.waitForFinished());
+
+    QByteArray output = sessionTool.readAllStandardOutput();
+    QVERIFY(output.startsWith("Error:"));
+
+    QString errorName = output.mid(6);
+    QCOMPARE(errorName, SIGNOND_PERMISSION_DENIED_ERR_NAME);
+}
+
 void SignondTest::testAuthSessionProcessUi()
 {
     QDBusMessage msg = methodCall(SIGNOND_DAEMON_OBJECTPATH,
-- 
2.13.0

