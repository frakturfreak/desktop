# Copyright 2010-2016 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="A GObject-based API for handling resource discovery and announcement over SSDP."
HOMEPAGE="https://wiki.gnome.org/Projects/GUPnP"

LICENCES="LGPL-2"
SLOT="1.0"
PLATFORMS="~amd64"
MYOPTIONS="gobject-introspection gtk3 gtk-doc"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? (
            dev-lang/vala:=[>=0.20]
            gnome-desktop/gobject-introspection:1[>=1.36.0]
        )
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/glib:2[>=2.32]
        gnome-desktop/libsoup:2.4[>=2.26.1]
        gtk3? ( x11-libs/gtk+:3[>=3.0] )
"

REMOTE_IDS="freecode:gupnp"

UPSTREAM_RELEASE_NOTES="http://ftp.gnome.org/pub/GNOME/sources/${PN}/$(ever range 1-2)/${PNV}.news"
UPSTREAM_DOCUMENTATION="http://developer.gnome.org/${PN}/$(ever range 1-2)"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'gtk3 gtk' )

