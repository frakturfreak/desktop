# Copyright 2008, 2009, 2010 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

require gtk-icon-cache freedesktop-desktop autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
#require option-renames [ renames=[ 'gtk3 providers:gtk3' ] ]

SUMMARY="Email client and news reader based on GTK+"
DESCRIPTION="
Claws Mail is an email client and news reader based on GTK+, featuring:
* Quick response
* Graceful and sophisticated interface
* Easy configuration and intuitive operation
* Abundant features
* Extensibility
* Robustness and stability
"
HOMEPAGE="http://www.claws-mail.org/"
DOWNLOADS="${HOMEPAGE}download.php?file=releases/${PNV}.tar.xz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"

# Claws-mail fails to compile with gtk3 as of 3.15.1 ( not specific to us, it seems )
# ( providers: gtk2 gtk3 ) [[ number-selected = exactly-one ]]
MYOPTIONS="
    archive         [[ description = [ Enable the archive plug-in ] ]]
    bogofilter      [[ description = [ Enable bogofilter anti-spam ] ]]
    dbus
    ldap
    libnotify
    networkmanager  [[ requires = dbus ]]
    pdf             [[ description = [ Enable PDF plug-in ] ]]
    pgp             [[ description = [ Enable PGP/SMIME plug-in ] ]]
    python          [[ description = [ Enable Python console/scripts plugin ] ]]
    rss [[ description = [ Enable RSS newsfeed plugin ] ]]
    spell
    startup-notification
    svg
    vcalendar       [[ description = [ Enable VCalendar/iCal plugin ] ]]
"

# providers:gtk2? ( x11-libs/gtk+:2[>=2.16] )
# providers:gtk3? ( x11-libs/gtk+:3[>=3.0] )
DEPENDENCIES="
    build:
        app-arch/xz
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-arch/libarchive
        dev-libs/glib:2[>=2.20]
        dev-libs/gnutls[>=3.0] [[ note = [ 3.0 for password encryption ] ]]
        mail-libs/libetpan[>=0.57] [[ note = [ required for imap ] ]]
        net-misc/curl
        x11-libs/cairo[>=1.0]
        x11-libs/gdk-pixbuf [[ note = [ automagic ] ]]
        x11-libs/gtk+:2[>=2.16]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/pango [[ note = [ automagic ] ]]
        archive? ( app-arch/libarchive )
        bogofilter? ( mail-filter/bogofilter )
        dbus? (
            dev-libs/dbus-glib:1[>=0.60]
            sys-apps/dbus[>=0.60]
        )
        ldap? ( net-directory/openldap )
        libnotify? (
            x11-libs/libnotify[>=0.4.3]
            media-libs/libcanberra[>=0.6][providers:gtk2?][providers:gtk3?]
        )
        networkmanager? ( net-apps/NetworkManager[>=0.6.2] )
        pdf? ( app-text/poppler[>=0.12][cairo(+)][glib] )
        pgp? ( app-crypt/gpgme[>=1.1.1] )
        python? (
            dev-lang/python:2.7
            gnome-bindings/pygtk:2[>=2.10.3]
        )
        rss? ( dev-libs/libxml2:2.0 )
        spell? ( app-spell/enchant[>=1.0.0] )
        startup-notification? ( x11-libs/startup-notification[>=0.5] )
        svg? ( gnome-desktop/librsvg:2[>=2.40.5] )
"

BUGS_TO="pioto@exherbo.org"

REMOTE_IDS="sourceforge:sylpheed-claws freecode:claws-mail"

UPSTREAM_CHANGELOG="http://git.claws-mail.org/?p=claws.git;a=blob_plain;f=NEWS;hb=HEAD"
UPSTREAM_DOCUMENTATION="
http://www.claws-mail.org/faq/index.php [[ description = [ FAQ ] ]]
http://www.claws-mail.org/documentation.php?section=general [[ description = [ User Manual ] ]]
"
UPSTREAM_RELEASE_NOTES="http://www.claws-mail.org/news.php"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    ### Plugins
    --enable-acpi_notifier-plugin
    --enable-archive-plugin
    --enable-address_keeper-plugin
    --enable-alternate-addressbook
    --enable-att_remover-plugin
    --enable-attachwarner-plugin
    --enable-fetchinfo-plugin
    --enable-mailmbox-plugin
    --enable-newmail-plugin
    --enable-spam_report-plugin
    --disable-bsfilter-plugin
    --disable-clamd-plugin
    --disable-demo-plugin
    --disable-fancy-plugin
    --disable-gdata-plugin
    --disable-libravatar-plugin
    --disable-notification-plugin
    --disable-perl-plugin
    --disable-spamassassin-plugin
    --disable-tnef_parse-plugin

    --disable-compface # missing dependency
    --disable-jpilot # missing dependency
    --disable-valgrind
    --enable-gnutls
    --enable-ipv6
    --enable-largefile
    --enable-libsm
    --enable-nls
    --enable-pthread
)
DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-multiarch-pkg-config-buildfix.patch
)

# 'providers:gtk3 gtk3'
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'archive archive-plugin'
    'bogofilter bogofilter-plugin'
    'dbus'
    'ldap'
    'libnotify notification-plugin'
    'networkmanager'
    'pdf pdf_viewer-plugin'
    'pgp pgpcore-plugin' 'pgp pgpmime-plugin' 'pgp pgpinline-plugin' 'pgp smime-plugin'
    'python python-plugin'
    'vcalendar vcalendar-plugin'
    'rss  rssyl-plugin'
    'spell enchant'
    'startup-notification'
    'svg'
)

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
}

