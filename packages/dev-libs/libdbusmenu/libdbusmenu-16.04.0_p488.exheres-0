# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PV##*_p}
UBUNTU_RELEASE=16.10

require launchpad test-dbus-daemon
require vala [ vala_dep=true with_opt=true option_name=vapi ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require option-renames [ renames=[ 'vala vapi' ] ]

SUMMARY="A small little library that was created from indicator-applet"
DOWNLOADS="https://bazaar.launchpad.net/~dbusmenu-team/libdbusmenu/trunk.${UBUNTU_RELEASE}/tarball/${MY_PNV} -> ${PNV}.tar.gz"

LICENCES="GPL-3 LGPL-2.1 LGPL-3"
SLOT="0.4"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk2
    gtk-doc
    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        app-doc/gtk-doc-autotools[>=1.14]
        core/json-glib[>=0.13.4]
        dev-libs/libxslt
        dev-util/intltool[>=0.35.0]
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.10] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-libs/atk
        dev-libs/glib:2[>=2.35.4]
        x11-libs/gtk+:3[>=2.91][gobject-introspection?]
        gtk2? ( x11-libs/gtk+:2[>=2.16][gobject-introspection?] )
"

# Needs X
RESTRICT="test"

BUGS_TO="keruspe@exherbo.org"

WORK=${WORKBASE}/\~dbusmenu-team/${PN}/trunk.${UBUNTU_RELEASE}

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PN}-gobject-introspection.patch )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-gtk
    --disable-dumper
    --disable-static
    --disable-tests
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    'vapi vala'
)

src_prepare() {
    edo sed -e 's/-Werror//' \
            -i libdbusmenu-glib/Makefile.am \
            -i libdbusmenu-gtk/Makefile.am \
            -i tests/Makefile.am \
            -i tools/Makefile.am \
            -i tools/testapp/Makefile.am

    edo gtkdocize --copy
    autotools_src_prepare
    edo intltoolize --automake
}

src_configure() {
    # Fix --disable-tests: configure: error: conditional "HAVE_VALGRIND" was never defined.
    export HAVE_VALGRIND_TRUE='#'
    export HAVE_VALGRIND_FALSE=''

    if option gtk2; then
        edo mkdir "${WORKBASE}"/gtk2-build
        edo cp -r "${WORK}"/* "${WORKBASE}"/gtk2-build
        edo pushd "${WORKBASE}"/gtk2-build

        local myconf=(
            "${DEFAULT_SRC_CONFIGURE_PARAMS[@]}" \
            $(for s in "${DEFAULT_SRC_CONFIGURE_OPTION_ENABLES[@]}" ; do \
                option_enable ${s} ; \
            done )
        )
        myconf+=( --with-gtk=2 )
        econf ${myconf[@]}

        edo popd
    fi

    default
}

src_compile() {
    if option gtk2; then
        edo pushd "${WORKBASE}"/gtk2-build
        default
        edo popd
    fi

    default
}

src_install() {
    if option gtk2; then
        edo pushd "${WORKBASE}"/gtk2-build
        default
        edo popd
    fi

    default
}

