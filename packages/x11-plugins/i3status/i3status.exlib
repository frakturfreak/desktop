# Copyright 2011-2012 Quentin "Sardem FF7" Glidic <sardemff7+exherbo@sardemff7.net>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_compile

SUMMARY="A small generator for status bars"
DESCRIPTION="
i3status is a small program (about 1500 SLOC) for generating a status bar for dzen2, xmobar or
similar programs. It is designed to be very efficient by issuing a very small number of system
calls, as one generally wants to update such a status line every second. This ensures that even
under high load, your status bar is updated correctly. Also, it saves a bit of energy by not
hogging your CPU as much as spawning the corresponding amount of shell commands would.
"
HOMEPAGE="http://i3wm.org/i3status"

LICENCES="BSD-3"
SLOT="0"

MYOPTIONS="
    pulseaudio
"

DEPENDENCIES="
    build:
        app-doc/asciidoc
    build+run:
        dev-libs/confuse
        dev-libs/yajl
        net-libs/libnl
        sys-sound/alsa-lib
        pulseaudio? ( media-sound/pulseaudio )
"

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX=/usr/$(exhost --target)
    MANPREFIX=/usr
    SYSCONFDIR=/etc
)

i3status_src_compile() {
    emake \
        "${DEFAULT_SRC_COMPILE_PARAMS[@]}" \
        $(option pulseaudio "" "NO_PULSEAUDIO=1")
}

