# Copyright 2013 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=djvu ] \
    freedesktop-desktop \
    gtk-icon-cache \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Portable djvu viewer"
DOWNLOADS="mirror://sourceforge/djvu/DjView/$(ever range 1-2)/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="tiff"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools:5 [[ note = [ lrelease/lconvert for translations ] ]]
        || (
            media-gfx/ImageMagick
            media-gfx/GraphicsMagick
            gnome-desktop/librsvg:2
            media-gfx/inkscape
        ) [[ note = [ for icon generation in desktopfiles/Makefile.in ] ]]
    build+run:
        app-text/djvu
        x11-apps/xdg-utils
        x11-libs/qtbase:5[gui]
        tiff? ( media-libs/tiff )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-desktopfiles
    --disable-nsdejavu # nsdejavu browser plugin
    --disable-npdjvu   # npdjvu browser plugin
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( tiff )

src_prepare() {
    # TODO: report upstream
    edo sed \
        -e "s:\(AC_PATH_PROG(PKG_CONFIG, \)\(pkg-config\):\1${PKG_CONFIG}:" \
        -i config/acinclude.m4

    edo sed \
        -e 's/Exec=djview4/Exec=djview/' \
        -i desktopfiles/djvulibre-djview4.desktop

    autotools_src_prepare
}

src_configure() {
    # no configure switch to force Qt5, last checked: 4.10.6
    edo export QMAKESPEC=/usr/$(exhost --target)/lib/qt5/mkspecs/linux-g++
    edo export QMAKE=/usr/$(exhost --target)/lib/qt5/bin/qmake
    edo export LRELEASE=/usr/$(exhost --target)/lib/qt5/bin/lrelease
    edo export LUPDATE=/usr/$(exhost --target)/lib/qt5/bin/lupdate

    default
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

