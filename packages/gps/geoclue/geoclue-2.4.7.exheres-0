# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2013 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="GeoClue location framework"
DESCRIPTION="
A software framework which can be used to enable geospatial awareness in applications.  GeoClue uses
DBus to provide location information.
"
HOMEPAGE="http://geoclue.freedesktop.org/"
DOWNLOADS="http://www.freedesktop.org/software/${PN}/releases/${PV:0:3}/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="2.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc networkmanager"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        sys-devel/gettext
        virtual/pkg-config[>=0.22]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        core/json-glib[>=0.14]
        dev-libs/glib:2[>=2.34.0]
        gnome-desktop/libsoup:2.4[>=2.42]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-3g-source
    --disable-cdma-source
    --disable-modem-gps-source
    --disable-nmea-source
    --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gobject-introspection introspection' )

